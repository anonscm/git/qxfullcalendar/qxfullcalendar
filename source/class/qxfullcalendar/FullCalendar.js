var FULLCALENDAR_ID = 0;

/**
 * A qooxdoo wrapper for the FullCalendar JavaScript Event Calendar library.
 *
 * The wrapper assumes that the files needed for execution are declared in the
 * header of the index.html file.
 * <pre class='html'>
 * &lt;link href='/fullcalendar/fullcalendar.css' rel='stylesheet' /&gt;
 * &lt;script src='/fullcalendar/lib/jquery.min.js'&gt;&lt;/script&gt;
 * &lt;script src='/fullcalendar/lib/moment.min.js'&gt;&lt;/script&gt;
 * &lt;script src='/fullcalendar/fullcalendar.js'&gt;&lt;/script&gt;
 * </pre>
 *
 * Usage:
 * <pre class='javascript'>
 * var calWin = new qx.ui.window.Window("Calendar").set({width: 600, height: 500});
 * calWin.setLayout(new qx.ui.layout.Grow());
 * var cal = new qxfullcalendar.FullCalendar();
 * calWin.add(cal);
 * calWin.moveTo(20, 50);
 * calWin.show();
 *
 * cal.execute('renderEvent', {title: 'test event', start: '2016-05-27T13:30-15:10'}, true);
 * cal.execute('gotoDate', '2016-05-27T13:30-15:10');
 * </pre>
 *
 * See the documentation of FullCalendar to know what the execute methode can do.
 * http://fullcalendar.io/docs/
 *
 * @ignore($)
 */
qx.Class.define("qxfullcalendar.FullCalendar", {
  extend : qx.ui.core.Widget,

  /**
   * Construct the qxfullcalendar.FullCalendar object.
   * @param conf {Object} A javascript Object describing the initial configuration
   * of the FullCalendar widget.
   */
  construct : function(conf) {
    this.base(arguments);
    FULLCALENDAR_ID++;
    this.setBackgroundColor("white");
    if (!conf) {
      conf = {};
    }
    qx.lang.Object.mergeWith(conf, qxfullcalendar.FullCalendar.DEFAULT_OPTIONS, false);

    // convertion of FullCalendar events
    var that = this;

    conf.viewRender = function(v, el) {
      that.fireDataEvent("viewRender", v);
    };
    conf.dayClick = function(d, e, v) {
      that.fireDataEvent("dayClick", d);
    };
    conf.eventClick = function(e) {
      that.fireDataEvent("eventClick", e);
    };
    conf.eventMouseover = function(e) {
      that.fireDataEvent("eventMouseover", e);
    };
    conf.eventMouseout = function(e) {
      that.fireDataEvent("eventMouseout", e);
    };

    this.__createCalendar(conf);
  },

  statics : {
    /**
     * Default options for FullCalendar. They get merged (non overwriting to the FullCalenda object).
     */
    DEFAULT_OPTIONS: {
      //selectable: true
      header: false,
      timeFormat: 'H(:mm)'
    }
  },

  events : {
    /**
     * Fired when a new date-range is rendered, or when the view type switches.
     */
    viewRender : "qx.event.type.Data",

    /**
     * Fired when the FullCalendar widget has been created.
     */
    calendarCreated : "qx.event.type.Event",

    /**
     * Fired when a day is clicked.
     */
    dayClick : "qx.event.type.Data",

    /**
     * Fired when an event is clicked.
     */
    eventClick : "qx.event.type.Data",

    /**
     * Fired when the pointer overed a event.
     */
    eventMouseover : "qx.event.type.Data",

    /**
     * Fired when the pointer leave an event.
     */
    eventMouseout : "qx.event.type.Data"
  },

  members : {
    /**
     * The DOM node where the calendar is build
     */
    __fcNode : null,

    /**
     * Get the FullCalendar DOM node
     * @return {DOM_node} the raw DOM node.
     */
    getFullCalendarNode : function() {
      return this.__fcNode;
    },

    /**
     * Calls the 'fullCalendar' methode on the FullCalendar DOM node.
     * Take arguments passed to the fullCalendar() methode (http://fullcalendar.io/docs/).
     */
    execute : function() {
      if (this.__fcNode != null) {
        if (arguments.length > 0) {
          if (!qx.lang.Type.isString(arguments[0])) {
            arguments = arguments[0];
          }
          this.__fcNode.fullCalendar.apply(this.__fcNode, arguments);
        }
      } else {
        this.addListenerOnce('calendarCreated', qx.lang.Function.bind(this.execute, this, arguments), this);
      }
    },

    /**
     * Returns the title of the current view.
     * @return {String} The title of the current view.
     */
    getTitle : function() {
      return this.execute('getView').title;
    },

    /**
     * Effectively create the FullCalendar widget
     * @param conf {Object} The configuration used to instanciate the calendar.
     */
    __createCalendar : function(conf) {
      this.addListenerOnce("appear", function(e) {
        var cssId = 'calendar-' + FULLCALENDAR_ID;
        var el = this.getContentElement().getDomElement();
        qx.bom.element.Attribute.set(el, 'id', cssId);
        qx.bom.element.Style.setStyles(el, qx.theme.manager.Font.getInstance().resolve('default').getStyles(), true);
        el = $('#' + cssId).fullCalendar(conf);
        this.__fcNode = el;

        this.addListener("calendarCreated", function() {
          this.__fcNode.fullCalendar('option', 'height', this.getBounds().height);
        }, this);

        this.addListener("resize", function(e) {
          this.__fcNode.fullCalendar('option', 'height', e.getData().height);
        }, this);

        this.fireEvent("calendarCreated");
      }, this);
    }
  }
});
