/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

/**
 * This is the main application class of your custom application "qxfullcalendar"
 *
 * @asset(qxfullcalendar/*)
 */
qx.Class.define("qxfullcalendar.Application",
{
  extend : qx.application.Standalone,



  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /**
     * This method contains the initial application code and gets called 
     * during startup of the application
     * 
     * @lint ignoreDeprecated(alert)
     */
    main : function()
    {
      // Call super class
      this.base(arguments);

      // Enable logging in debug variant
      if (qx.core.Environment.get("qx.debug"))
      {
        // support native logging capabilities, e.g. Firebug for Firefox
        qx.log.appender.Native;
        // support additional cross-browser console. Press F7 to toggle visibility
        qx.log.appender.Console;
      }

      /*
      -------------------------------------------------------------------------
        Below is your actual application code...
      -------------------------------------------------------------------------
      */

      var btnNext = new qx.ui.form.Button("next");
      var btnNewEvent = new qx.ui.form.Button("new event");
      var conf = {
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        eventLimit: true,
        events: [
        {
          title: "test",
          start: "2016-05-24T10:00",
          end: "2016-05-24T11:30"
        }
        ]
      };
      var cal = new qxfullcalendar.FullCalendar({});
      var calWin = new qx.ui.window.Window("Calendar").set({width: 600, height: 500});
      calWin.setLayout(new qx.ui.layout.Grow());
      var doc = this.getRoot();
      doc.add(btnNext, {left: 20, top: 5});
      doc.add(btnNewEvent, {left: 100, top: 5});
      calWin.add(cal);
      calWin.moveTo(20, 50);
      calWin.show();
      cal.execute('renderEvent', {title: "test event", start: "2016-05-27T13:30-15:10"}, true);
      btnNext.addListener("execute", function() {
        cal.execute("next");
      }, this);
      btnNewEvent.addListener("execute", function() {
        cal.execute('renderEvent', {title: "new event", start: "2016-05-23T12:00"}, true);
      }, this);

      cal.addListener("viewRender", function(e) {
        calWin.setCaption("My calendar - " + e.getData().title);
      }, this);
      cal.addListener("dayClick", function(e) {
        this.debug("day clicked: " + e.getData().format());
      }, this);
      cal.addListener("eventClick", function(e) {
        this.debug("event clicked: " + e.getData());
      }, this);
      cal.addListener("eventMouseover", function(e) {
        this.debug("event mouseover: " + e.getData());
      }, this);
      cal.addListener("eventMouseout", function(e) {
        this.debug("event mouseout: " + e.getData());
      }, this);
    }
  }
});
