/** <h3> qxfullcalendar API Documentation </h3>
 *
 * This is a tiny qooxdoo wrapper for the FullCalendar library.
 *
 * To use this make sure to have fullcalendar available at your
 * web server root: http://your.server/fullcalendar
 *
 */
