/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("qxfullcalendar.theme.Theme",
{
  meta :
  {
    color : qxfullcalendar.theme.Color,
    decoration : qxfullcalendar.theme.Decoration,
    font : qxfullcalendar.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : qxfullcalendar.theme.Appearance
  }
});